/*
* @file main.cpp
* @author Emilia Paredes
*         540001917
*         m.paredes@digipen.edu
* /date 2020 / 03 / 27
* /brief main function and couple of helper dummy functions in order to test profiler.
* /copyright Copyright(C) 2020 DigiPen Institute of Technology.
*/

//include profiler
#include "profiler.h"
//get a "global" instance of the profiler
profiler_::Profiler* _profiler = profiler_::Profiler::GetProfiler();

//dummy function #2
void nested_func() {
	_profiler->StartFrame("Nested");
	for (int j = 0; j < 100; j++) {
	}
	_profiler->EndFrame("Nested");
}

//dummy function #1
void other_func() {
	_profiler->StartFrame("SecondFunc");
	for (int i = 0; i < 1000; i ++) {
		_profiler->Deactivate();
		nested_func();
		_profiler->Activate();
		for (int j = 0; j < 100; j++) {
		}
	}
	_profiler->EndFrame("SecondFunc");
}

//main function, includes call to other functions
int main(){
	//just a counter 
	int current = 0;
	while (true){
		//first node
		_profiler->StartFrame("Start");
		other_func();
		_profiler->EndFrame("Start");
		//ending condition
		current++;
		if (current > 100)
			break;
	}

	//dump to text
	Formatter::Dump(_profiler->GetStats(), "profiler_statistics.txt");
	return 0;
}