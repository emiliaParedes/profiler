CS315 - Profiler
M. Emilia Paredes C. 

- How was the overall experience with the assignment?
Personally I found it really confusing, at first I had no idea what was expected from the program therefore I 
wasn't able to understand where to begin with. After understanding the function of the assignment I could finally 
start working but everytime I tackled something I encountered some issue about application, should I handle this case
or not? and that happened several times so it slowed the production. Also I had some trouble with the structure of 
the assignment as tought in class, I didn't like the structure given with a child pointer node and sibling node pointer,
I found it as dependent and I didn't like so I tried to implement it my way and it took me more time that what I would have 
spent doing the structure taught, still I really like the way I was able to implement it. 
Besides that I think it was fun to do this, it was kind of a challenge so I liked it.

How long did the assignment take?
It took me around 10 hours.

What would you say you took away from doing it? In other words, what did you learn from it?
By doing my own implementation, I think I learned more ways of doing one task and it was really fullfiling. 

What was the hardest part about it?
Understand all the time what I had to do.

Is there anything in particular you would change (to improve the assignment experience as a learning device)?
More depth description in the cases to be handled. For example, distinguish the function being called fromm diferent
functions in order to get diferent informaiton. 

Additional comments or suggestions.

