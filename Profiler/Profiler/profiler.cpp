/*
* @file profiler.cpp
* @author Emilia Paredes
*         540001917
*         m.paredes@digipen.edu
* /date 2020 / 03 / 27
* /brief Profler class function definitions and formatter class function definitions
* /copyright Copyright(C) 2020 DigiPen Institute of Technology.
*/

#ifdef COMPILE_PROFILER

#include "profiler.h"
#include <intrin.h>
#include <cassert>

#ifdef DUMP_TO_IMGUI
#include <ImGui\imgui.h>	//imgui
#include <sstream>
#endif

#ifdef DUMP_TO_TEXT
#include <fstream>
#include <sstream>
#endif 

//to get number of cycles
#pragma intrinsic(__rdtsc)

/**
* Namespace ImGuiDump
* /brief	namespace that contains helper functions when dumping stats to ImGui
*/
namespace ImGuiDump {
	/**
	* traverse_children - given a NodeInfo, and all the nodes,
	* /brief	namespace that contains helper functions when dumping stats to ImGui
	*/
	void traverse_children(const profiler_::NodeInfo& current, const std::map < std::string, profiler_::NodeInfo > & all_nodes);
}

/**
* Namespace profiler_
* /brief	namespace that contains class profiler and helper classes and definitions
*/
namespace profiler_ {
	//initialize static singleton 
	Profiler* Profiler::m_instance = nullptr;

	/**
	* GetProfiler - a nice way to make it a singleton, returns the instance of the class
	*/
	Profiler* Profiler::GetProfiler() {
		if (m_instance == nullptr)
			m_instance = new Profiler{};
		return m_instance;
	}

	/**
	* StartFrame - Sets when a node is being created
	* /param id - the id that corresponds to created node
	*/
	void Profiler::StartFrame(const std::string& id) {
		//check if active or not, if not, do not gather information 
		if (!m_active)
			return;
		//get initial time
		if (m_nodes.size() == 0)
			m_init_time = __rdtsc();

		//check if we had add it before 
		auto found = m_nodes.find(id);
		//if we havent called this function then we add it to the map of nodes
		if (found == m_nodes.end()) {
			m_nodes.insert({ id,{} });
			m_nodes[id].id = id;
			//check if it has parent
			if (!m_active_nodes.empty())
				m_nodes[m_active_nodes.top()].children.push_back(id);
		}

		if (!m_active_nodes.empty()) {
			m_nodes[id].parents.insert({ m_active_nodes.top(),{ 0, 0.0f } });
			m_nodes[id].parents[m_active_nodes.top()].first++;
		}
		else {
			m_nodes[id].parents.insert({ "Root",{ 0, 0.0f } });
			m_nodes[id].parents["Root"].first++;
		}
		//set active nodes
		m_active_nodes.push(id);

		//increase the number of calls of this node
		m_nodes[id].calls++;
		//set nodes start time
		m_nodes[id].start_time = __rdtsc();
	}

	/**
	* EndFrame - Sets when a node is being deleted/ended
	* /param id - the id that corresponds to the node
	*/
	void Profiler::EndFrame(const std::string& id) {
		//check if active or not, if not, do not gather information 
		if (!m_active)
			return;
		//assert no one misplace the end call 
		assert(m_nodes.find(id) != m_nodes.end());
		//get time it took to end this frame
		unsigned long long current_frame = __rdtsc() - m_nodes[id].start_time;
		//get current frame time 
		m_nodes[id].time += current_frame;

		//check if this frame is the smallest one || the first one
		if (current_frame < m_nodes[id].min_frame[1] || m_nodes[id].min_frame[0] == 0) {
			m_nodes[id].min_frame[0] = m_nodes[id].calls;
			m_nodes[id].min_frame[1] = current_frame;
		}

		//check if this frame is the biggest one || the first one
		if (current_frame > m_nodes[id].max_frame[1] || m_nodes[id].max_frame[0] == 0) {
			m_nodes[id].max_frame[0] = m_nodes[id].calls;
			m_nodes[id].max_frame[1] = current_frame;
		}
		//set current start time at 0 so we know we have finished this frame 
		m_nodes[id].start_time = 0;

		//delete from active nodes 
		m_active_nodes.pop();
	}

	/**
	* Activate - activates profiler to gather information
	*/
	void Profiler::Activate() {
		m_active = true;
	}

	/**
	* Deactivate - deactivates profiler to not gather information
	*/
	void Profiler::Deactivate() {
		m_active = false;
	}

	/**
	* GetStats - Calculates the final statitics from the nodes given
	*/
	ProfilerStats Profiler::GetStats() {
		ProfilerStats stats{};
		m_total_time = __rdtsc() - m_init_time;
		//stats.cycles = m_cycles;
		stats.time = m_total_time;
		stats.total_nodes = m_nodes.size();
		//compute the information about percentages respect to parent
		for (auto& node : m_nodes) {
			//iter through parents and check
			for (auto& parent : node.second.parents) {
				if (parent.first == "Root") {
					parent.second.second = node.second.time * 100.0f / m_total_time;
					continue;
				}
				if (m_nodes[parent.first].time == 0)
					continue;
				unsigned long long time_by_call = node.second.time * parent.second.first / node.second.calls;
				parent.second.second = time_by_call * 100.0f / m_nodes[parent.first].time;
			}
		}

		stats.nodes = m_nodes;

		return stats;
	}

	/**
	* operator<< - Overloades operator << to output the information of the node
	* /param os - the ostream where the information will be dumped
	* /param node_info - the object that contains the information
	*/
	std::ostream & operator<<(std::ostream & os, const NodeInfo & node_info) {
		os << "*********** Node " << node_info.id.c_str() << " ***********" << std::endl;
		os << "** Total Cycles:            " << node_info.time << std::endl;
		os << "** Calls Made:              " << node_info.calls << std::endl;
		os << "** Average:                 " << node_info.time / node_info.calls << std::endl;
		os << "** Min Frame[" << node_info.min_frame[0] << "]:           " << node_info.min_frame[1] << std::endl;
		os << "** Max Frame[" << node_info.max_frame[0] << "]:           " << node_info.max_frame[1] << std::endl;

		os << "** Called by:               " << std::endl;
		for (auto& node : node_info.parents) {
			os << "	** " << node.first.c_str() << std::endl;
			os << "		** Percentage from Parent:  " << node.second.second << "%" << std::endl;
		}

		os << "** Calling: " << std::endl;
		for (size_t i = 0; i < node_info.children.size(); i++)
			os << "		** " << node_info.children[i].c_str() << std::endl;
		return os;
	}

	/**
	* operator<< - Overloades operator << to output the statistics of the profiler
	* /param os - the ostream where the information will be dumped
	* /param stats - the object that contains the information
	*/
	std::ostream & operator<<(std::ostream & os, const ProfilerStats & stats) {
		os << "------------------ Profiler General Stats ------------------ " << std::endl;
		os << "-- Total Cycles:            " << stats.time << std::endl;
		os << "-- Total Nodes :            " << stats.total_nodes << std::endl;
		os << "------- Node Information ---------" << std::endl;

		for (auto& iter : stats.nodes)
			os << iter.second;

		os << "----------------------------------------------------------- " << std::endl;
		return os;
	}
}

/**
* Static variables created when dumping to imgui
*/
// max frames to take into account
unsigned Formatter::max_frames = std::numeric_limits<unsigned>::max();
// current frames passed
unsigned Formatter::current_frames = 0;
// to decide when to stop accumulating information
bool Formatter::stop = false;
// if we set maz number of frames
bool Formatter::set_max_frames = false;
//in order to keep track of the previous stats gotten (qhen stop or max frames reached)
profiler_::ProfilerStats Formatter::prev{};

/**
* Dump - given a profiler stats outputs the information in the desired way depending on
*		define (DUMP_TO_TEXT, DUMP_TO_IMGUI, DUMP_TO_CONSOLE)
* /param stats - stats to print
* /param file_out - optional parameter when dumping to text, defines output files name
*/
void Formatter::Dump(const profiler_::ProfilerStats & stats, const char* file_out) {
	#ifdef DUMP_TO_TEXT
	//open file to write 
	std::ofstream file(file_out, std::ofstream::out);
	if (!file.is_open()) {
		std::cout << "File couldn't be opened, line: " << __LINE__ << std::endl;
		return;
	}
	file << stats;
	file.close();
	#endif //DUMP_TO_TEXT

	#ifdef DUMP_TO_IMGUI
	ImGui::SetNextWindowPos(ImVec2(20, 20), ImGuiCond_FirstUseEver);
	current_frames++;
	bool show = true;
	ImGui::Begin("Profiler Statistics", &show, {});
	//ImGui::Text();
	//ImGui::PlotLines("Cycles", cycles, IM_ARRAYSIZE(cycles), 0, nullptr, 500.0f, 10000.0f, ImVec2(500.0f, 50.0f));
	ImGui::Checkbox("Stop", &stop);
	if ((set_max_frames && current_frames > max_frames))
		stop = true;
	if (!stop)
		prev = stats;

	ImGui::TextColored(ImVec4(0.0f, 0.5f, 0.7f, 1.0f), "Frames Passed: %d", current_frames);

	(ImGui::Checkbox("Set number of frames", &set_max_frames));
	int max = static_cast<int>(max_frames);
	ImGui::DragInt("MaxFrames", &max);
	max_frames = static_cast<unsigned>(max);


	ImGui::NewLine();

	if (ImGui::CollapsingHeader("All Nodes")) {
		for (auto& iter : prev.nodes) {
			for (auto& parent : iter.second.parents) {
				if (parent.first == "Root")
					ImGuiDump::traverse_children(iter.second, prev.nodes);
			}
		}
	}
	ImGui::End();
	#endif // DUMP_TO_IMGUI

	#ifdef DUMP_TO_CONSOLE
	std::cout << stats << std::endl;
	#endif // DUMP_TO_CONSOLE

}


namespace ImGuiDump {
	void traverse_children(const profiler_::NodeInfo& current, const std::map < std::string, profiler_::NodeInfo > & all_nodes) {
		#ifdef DUMP_TO_IMGUI
		//just to change color of title
		ImGui::PushStyleColor(0, ImVec4(0.0f, 0.8f, 0.7f, 1.0f));
		bool open = ImGui::TreeNode(current.id.c_str());
		ImGui::PopStyleColor();
		//check if the node is open 
		if (open) {
			//current node information
			std::stringstream stats_string{};
			stats_string << current;
			std::string final_string = stats_string.str();
			ImGui::Text(final_string.c_str());
			//get to children
			ImGui::PushStyleColor(0, ImVec4(0.0f, 0.8f, 0.7f, 1.0f));
			if (current.children.size() != 0)
				ImGui::Text("**** Children: ");
			ImGui::PopStyleColor();
			for (auto& children : current.children)
				ImGuiDump::traverse_children(all_nodes.at(children), all_nodes);
			ImGui::TreePop();
		}
		#endif	//DUMP_TO_IMGUI
	}
}

#endif