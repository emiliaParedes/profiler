/*
* @file profiler.h
* @author Emilia Paredes
*         540001917
*         m.paredes@digipen.edu
* /date 2020 / 03 / 27
* /brief Profiler class function definitions and formatter class function definitions
* /copyright Copyright(C) 2020 DigiPen Institute of Technology.
*/

#pragma once

#ifdef COMPILE_PROFILER

#include <iostream>
#include <map>
#include <utility>
#include <limits>
#include <stack>
#include <vector>

namespace profiler_ {

	using NodeParent = std::pair<std::string, unsigned>;
	using ParentInfo = std::pair<unsigned, float>;

	struct NodeInfo {
		std::string id{};
		unsigned long long start_time = 0;
		unsigned long long time = 0;
		unsigned calls = 0;
		unsigned long long min_frame[2] = { 0, std::numeric_limits<unsigned>::max() };
		unsigned long long max_frame[2] = { 0, std::numeric_limits<unsigned>::min() };
		std::vector<std::string> children{};
		std::map<std::string, ParentInfo> parents{};
		friend std::ostream& operator<<(std::ostream& os, const NodeInfo& node_info);
	};

	struct ProfilerStats {
		unsigned long long time;
		size_t total_nodes;
		std::map<std::string, NodeInfo> nodes;
		friend std::ostream& operator<<(std::ostream& os, const ProfilerStats& stats);
	};

	class Profiler {
	public:
		static Profiler* GetProfiler();
		void StartFrame(const std::string& id);
		void EndFrame(const std::string& id);
		void Activate();
		void Deactivate();
		ProfilerStats GetStats();
		Profiler(Profiler const&) = delete;
		void operator=(Profiler const&) = delete;
	private:
		bool m_active = true;
		static Profiler* m_instance;
		Profiler() {};
		std::stack<std::string> m_active_nodes{};
		std::map<std::string, NodeInfo> m_nodes{};
		unsigned long long m_init_time{};
		unsigned long long m_total_time{};
	};
}

class Formatter {
public:
	static void Dump(const profiler_::ProfilerStats & stats, const char* file_out = "profiler_statistics.txt");
private:
	static unsigned max_frames;
	static unsigned current_frames;
	static bool stop;
	static bool set_max_frames;
	static profiler_::ProfilerStats prev;
};

#endif
